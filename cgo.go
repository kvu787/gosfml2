// Copyright (C) 2012-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package gosfml2

// #cgo LDFLAGS: -L/CSFML-2.1/lib/gcc -lcsfml-window -lcsfml-graphics -lcsfml-audio
// #cgo CFLAGS: -I/CSFML-2.1/include -I/CSFML-2.1/lib/gcc
import "C"
